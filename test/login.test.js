const request = require('supertest');
const expect = require('chai').expect;
const app = require('../app');

describe('GET /', () => {
	it('Debería retornar la página de login', async () => {
		const res = await request(app).get('/');
		expect(res.status).equal(200);
		expect(res.text).contains('Iniciar Sesión');
	});
});

describe('POST /login', () => {
	it('Debería validar que sean sólo números', async () => {
		const res = await request(app).post('/login').send({
			username: '72307623a',
			password: '123456'
		});
		expect(res.status).equal(400);
		expect(res.text).equal('El nombre de usuario debe contener sólo números');
	});

	it('Debería iniciar exitosamente', async () => {
		const res = await request(app).post('/login').send({
			username: '72307623',
			password: 'n1t3r%p457&'
		});
		expect(res.status).equal(200);
		expect(res.text).contains('Productos');
	});

	it('Debería rechazar inicio de sesión', async () => {
		const res = await request(app).post('/login').send({
			username: '72307623',
			password: 'n1t3r%p457&#'
		});
		expect(res.status).equal(401);
		expect(res.text).contains('No autorizado');
	});

	it('Debería asegurar que contraseña sea válida', async () => {
		const res = await request(app).post('/login').send({
			username: '72307623',
			password: 'n1t3r%p457&'
		});
		expect(res.status).equal(200);
		expect(res.text).contains('Productos');
	});

	it('Debería asegurar que contraseña sin repetir caracteres', async () => {
		const res = await request(app).post('/login').send({
			username: '72307623',
			password: 'n1t333p457&'
		});
		expect(res.status).equal(400);
		expect(res.text).contains('La contraseña no debe contener caracteres repetidos');
	});
})