const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/index.html');
});

app.post('/login', (req, res) => {
	console.log(req.body);
	let username = req.body.username;
	let password = req.body.password;

	let regexUsername = /^\d{6,}$/;
	let regexPassword = /^(?=.*\d)(?=.*[a-zA-Z]).{10,}$/;

	if(!regexUsername.test(username)) {
		regexUsername.lastIndex = 0;
		res.status(400).send('El nombre de usuario debe contener sólo números');
		return;
	}

	charsetPassword = [...new Set(password.split(''))];

	console.log(charsetPassword);

	if(charsetPassword.join('') !== password) {
		res.status(400).send('La contraseña no debe contener caracteres repetidos');
		return
	}

	if(!regexPassword.test(password)) {
		regexUsername.lastIndex = 0;
		res.status(400).send('La contraseña debe contener como mínimo un número, una letra y un caracter especial');
		return
	}

	if(username === '72307623' && password === 'n1t3r%p457&') {
		res.send(`
			Productos: <br>
			- Manzanas <br>
			- Correas <br>
			- Calcetines con diseño <br>
			- Camisetas <br>
		`);
	} else {
		res.status(401).send('No autorizado');
	}
});

module.exports = app;